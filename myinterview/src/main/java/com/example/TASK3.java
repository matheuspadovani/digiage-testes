package com.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */
public class TASK3 {
    
    public static void main(String[] args){
    
        int listSize=(int)(Math.random()*10000);//gera número aleatório de 1 a 10000
     //int listSize =50;
        ArrayList<String> list= new ArrayList<String>();
        for (int j=0;j<listSize;j++){

            int size=3; //a escolha de 3 caracteres força a ter strings repetidas
            String CaracterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
            StringBuilder sbuilder=new StringBuilder(size);
                for (int i=0;i<size;i++){
                    int caracter;
                    caracter = (int) (Math.random()*CaracterString.length());
                    sbuilder.append(CaracterString.charAt(caracter));//cria strings aleatórias com as letras
                }
                list.add(sbuilder.toString());
        }      
        
        Set unique=new HashSet(list);
        System.out.println("tamanho da lista com repetidos= "+ list.size());
        System.out.println("tamanho da lista com valores únicos= "+ unique.size());
        
        
        printlista(list);
        printlista(unique);
     }
    
    
     private static void printlista(Collection x){
        for (Object i : x){
            System.out.print(i+" ");
         }
         System.out.print("\n");
    }
}
