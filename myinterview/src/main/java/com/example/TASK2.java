package com.example;

/**
 * Task here is to write a list. Each element must know the element before and
 * after it. Print out your list and them remove the element in the middle of
 * the list. Print out again.
 *
 * 
 */
public class TASK2 {

    
    
    public void Task2(){
     DoubleLinkedList dblist = new DoubleLinkedList();
        int listSize=15;
           for (int j=0;j<listSize;j++){
               int size=8;
               String CaracterString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                           + "0123456789"
                                           + "abcdefghijklmnopqrstuvxyz";
               StringBuilder sbuilder=new StringBuilder(size);
                   for (int i=0;i<size;i++){
                       int caracter;
                       caracter = (int) (Math.random()*CaracterString.length());
                       sbuilder.append(CaracterString.charAt(caracter));
                   }
                String nome=sbuilder.toString();
                dblist.insertEnd(nome);
            }
        dblist.print();
        while(dblist.getLength()>0)
        {
            dblist.removeMiddle();
            dblist.print();
        }       
        
        }
    } 

   class DoubleLinkedList {
    
        private ListNode head;
        private ListNode tail;
        private int length;
        
        public DoubleLinkedList(){        
        this.head=null;
        this.tail=null;
        this.length=0;
        }
        
        public int getLength()
        {
            return length;
        }
        
        public boolean isEmpty(){
        return length==0;
        }
        
        public void insertEnd(String value){
            try{
            ListNode newNode = new ListNode(value);
            //System.out.println("nó criado com sucesso  valor="+newNode.getText());
            if(isEmpty()){
                head=newNode;
                //System.out.println("vazio");
            }else{
                newNode.previus=tail;  
                tail.next=newNode;
            }
            tail=newNode;
            length++;
            }
            catch(Exception e) {
                System.err.println("erroinsert"+e);
            }
        }
        
        public void removeMiddle(){
             try{
                //System.out.println("tamanho da lista="+length);
                    if(length>2){
                      int pos=Math.round(length/2);
                      ListNode aux=head;
                      int countAux=0;
                      while (countAux<pos){
                          aux=aux.getNext();
                          countAux++;
                          }
                      //System.out.println(pos);
                      ListNode prox=aux.getNext();
                      ListNode ant=aux.getPrevius();

                      prox.setPrevius(aux.getPrevius());
                      ant.setNext(aux.getNext());
                      length--;
                    }else{
                    removeEnd();
                    length--;
                    }
                    }
            catch(Exception e) {
                System.err.println("erro remove"+e.getMessage());
            }
        }
            
        public void removeEnd(){
        ListNode aux=head;
        if(aux.getNext()!=null){
            aux=aux.getNext();
        }else{
        aux.setNext(null);
        ListNode ant=aux.previus;
        ant.setNext(aux.getNext());
        length--;
        }
        
        
        }
            
        public void print(){
            try{
        ListNode auxi=head;
        System.out.println("início da lista: ");
        for(int i=0; i<length;i++){
            System.out.println(auxi.getText());
            auxi=auxi.getNext();
        }
         }
            catch(Exception e) {
                System.err.println("erroprint"+e);
            }
        }
     
    
        
    public class ListNode {
   
    String text;
    ListNode previus;
    ListNode next;
    
    public ListNode(String text){
    this.text=text;
    }

        public ListNode getPrevius() {
            return previus;
        }

        public void setPrevius(ListNode previus) {
            this.previus = previus;
        }

        public ListNode getNext() {
            return next;
        }

        public void setNext(ListNode next) {
            this.next = next;
        }

        public String getText() {
            return text;
        }
        
}
       
}
  
    
            



