package com.example;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/*
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
public class TASK4 {
    private static HttpURLConnection connection;
   
    
    public static void main(String[] args) throws MalformedURLException, FileNotFoundException {
        
        BufferedReader reader;
        String line;
        StringBuffer responseContent= new StringBuffer();
        try{
            URL url= new URL("https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda");
            connection = (HttpURLConnection) url.openConnection();
            // request

            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            int status=connection.getResponseCode();
            //System.out.println(status);
        
            if(status>299){
                reader= new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while((line= reader.readLine())!=null){
                    responseContent.append(line);
                }
                reader.close();
            }else{//conexao funcionou
                reader= new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while((line= reader.readLine())!=null){
                    responseContent.append(line);
                }
            
            }
            System.out.println(responseContent.toString());
        }
        catch (MalformedURLException e){
        e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(TASK4.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            connection.disconnect();
        }
        int result[]= new int[2];
        result =parse(responseContent.toString());
        System.out.println(result[0]);
        System.out.println(result[1]);
        
        File myObj = null;
        try {
            myObj = new File("result.txt");
            if (myObj.createNewFile()) {     
            } else {
              System.out.println("File already exists.");
            }
            System.out.println("File created: " + myObj.getName());
                    try {
                    FileWriter fileWriter = new FileWriter("result.txt");
                    PrintWriter printWriter = new PrintWriter(fileWriter);
                    printWriter.println(
                            String.format("Total count of  M = %d"
                                    + " \n Total count of  F = %d",
                            result[0], result[1]));
                    printWriter.close();                    
                  System.out.println("Successfully wrote to the file.");
                          try {
                          Scanner myReader = new Scanner(myObj);
                              System.out.println("lendo o arquivo");
                          while (myReader.hasNextLine()){
                          String data = myReader.nextLine();
                          //System.out.println(data);
                          }
                            } catch (IOException e) {
                          System.out.println("An error occurred.");
                          e.printStackTrace();
                        }
       
                  
                } catch (IOException e) {
                  System.out.println("An error occurred.");
                  e.printStackTrace();
                }
            
            
            
            
            
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
          
          
          
        
      
        if (myObj!=null){
            //System.out.println("nao nulo");
            boolean upload = uploadObject(myObj);
            if(upload)
            {
                System.out.println("Arquivo enviado com sucesso");
            }
        }
        
        
        
        
    }
    
    public static int[] parse (String responseBody){
        JSONArray people = new JSONArray(responseBody);
        int f=0,m=0;
        for (int i=0; i<people.length();i++){
            JSONObject func = people.getJSONObject(i);
            String gender = func.getString("gender");
            if (gender.equals("F")){
            f++;}
            else{
            m++;}
            //System.out.println(gender);
        }
        System.out.println("M="+m);
        System.out.println("F="+f);
        int genderCount[]= new int[2];
                genderCount[0]=m;
                genderCount[1]=f;
        return genderCount;
        
    }

    
    public static boolean uploadObject(File fileName){

        Regions clientRegion = Regions.DEFAULT_REGION;
        String bucketName = "interview-digiage";
        //String stringObjKeyName = "string obj key name";
        String fileObjKeyName = "resultado.txt";

        AWSCredentials credentials = new BasicAWSCredentials(
                "AKIAU7BHLOLBEUKPFY5V",
                "aPoSLXGbRljVMAej8cumPsyI2S6zXtkVtzOnnNBX"
        );
        
        
        try {
            //This code expects that you have AWS credentials set up per:
            // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
            AmazonS3 s3Client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(clientRegion)
                    .build();
            
            // Upload a text string as a new object.
            //s3Client.putObject(bucketName, stringObjKeyName, "Uploaded String Object");

            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, fileName);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("title", "Task 4 Digiage Interview - Matheus Rosisca Padovani");
            request.setMetadata(metadata);
            s3Client.putObject(request);
            
            
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
            e.printStackTrace();
            return false;
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
            return false;
        }
        return true;
    }
}