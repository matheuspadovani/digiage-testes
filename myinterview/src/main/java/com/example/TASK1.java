package com.example;
import java.lang.StringBuilder;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
    
    private boolean isPalindrome(String word){
        
        String lowercase=word.toLowerCase().trim();
        StringBuilder backward= new StringBuilder();
        backward.append(lowercase);
        backward=backward.reverse();
        return (lowercase.equals(backward.toString()));
    }
    
    

}
